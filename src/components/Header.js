import React from 'react'
import Logo from './Logo'

const Header = () => (
    <header>
        <div className="f1">
            <button type='button'>
                <img src="/images/misc/user.png" alt="User Settings"/>
            </button>
        </div>

        <div className='f1'>
            <Logo/>
        </div>

        <div className='f1'>
            <button type="button">
                <img src="/images/misc/messages.png" alt="View Messages"/>
            </button>   
        </div>
    </header>
)

export default Header